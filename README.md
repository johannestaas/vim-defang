# vim-defang

## Installation

First, get the python dependency.

    pip install defang

Then, use your vim plugin manager of choice.

- [Pathogen](https://bitbucket.org/tpope/vim-pathogen)
  - `git clone https://bitbucket.org/johannestaas/vim-defang ~/.vim/bundle/vim-defang`
- [Vundle](https://bitbucket.org/gmarik/vundle)
  - Add `Bundle 'https://bitbucket.org/johannestaas/vim-defang'` to .vimrc
  - Run `:BundleInstall`
- [NeoBundle](https://bitbucket.org/Shougo/neobundle.vim)
  - Add `NeoBundle 'https://bitbucket.org/johannestaas/vim-defang'` to .vimrc
  - Run `:NeoBundleInstall`
- [vim-plug](https://bitbucket.org/junegunn/vim-plug)
  - Add `Plug 'https://bitbucket.org/johannestaas/vim-defang'` to .vimrc
  - Run `:PlugInstall`

## Usage

Run :Defang to defang all URLs and hostnames.

Run :Refang to refang them back.

Use visual mode and select a block, and run Defang or Refang to run on that block.
