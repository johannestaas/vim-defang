" --------------------------------
" Add our plugin to the path
" --------------------------------
python import sys
python import vim
python sys.path.append(vim.eval('expand("<sfile>:h")'))

" --------------------------------
"  Function(s)
" --------------------------------
function! Defang()
python << endOfPython

from vim_defang import defanger

vim.current.line = defanger(vim.current.range[0])

endOfPython
endfunction

function! Refang()
python << endOfPython

from vim_defang import refanger

vim.current.line = refanger(vim.current.range[0])

endOfPython
endfunction

" --------------------------------
"  Expose our commands to the user
" --------------------------------
command! -range=% Defang <line1>,<line2>call Defang()
command! -range=% Refang <line1>,<line2>call Refang()
