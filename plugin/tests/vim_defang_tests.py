import unittest
import vim_defang as sut


@unittest.skip("Don't forget to test!")
class VimDefangTests(unittest.TestCase):

    def test_example_fail(self):
        result = sut.vim_defang_example()
        self.assertEqual("Happy Hacking", result)
