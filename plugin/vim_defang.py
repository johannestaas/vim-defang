from defang import defang, refang

def defanger(line):
    return defang(line)

def refanger(line):
    return refang(line)
